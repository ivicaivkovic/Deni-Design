<?php 

include("config.php");
session_start();

if(!isset($_SESSION['login_user'])){
	header("location: login.php");
}

$date = "";
$freeText = "";
$category = "";
$category1 = "";
$image = "";
$dateFrom = "";
$dateTo = "";

$target_dir = "images/";

// If entry save button clicked
if(isset($_POST['saveEntry'])){
	$date = $_POST['date'];
	$freeText = $_POST['freeText'];
	$imageFile = $_FILES['image']['name'];
	$category = $_POST['category'];

	$target_file = $target_dir . basename($_FILES["image"]["name"]);

	// Select file type
	$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

	// Valid file extensions
	$extensions_arr = array("jpg","jpeg","png","gif");

	// Check extension
	if( in_array($imageFileType,$extensions_arr) ){

		$image = $imageFile;

		// Upload file
		if (!file_exists($target_file))
			move_uploaded_file($_FILES['image']['tmp_name'],$target_dir.$imageFile);

	}else{

		$imgError = "Not supported image format, please upload jpg, png, gif or jpeg file!";

	}

	if(!empty($_POST['date']) && !empty($_POST['freeText'])){

		// Insert record
		$query = "INSERT into entries (date, freeText, category, image, userId) values ('".$date."','".$freeText."','".$category."','".$image."', ".$_SESSION['user_id'].")";
	    mysqli_query($db,$query);

	    $_SESSION['entryAdded'] = true;
	    $showMsg = true;
	}else{
		$emptyFieldError = "Please add date and text to an entry!";
	}

}

// Save values after form submit
if(isset($_POST['filterDate'])){
	$dateFrom = $_POST['dateFrom'];
	$dateTo = $_POST['dateTo'];
	$category1 = "";
}

// Save values after form submit
if(isset($_POST['filterCategory'])){
	$category1 = $_POST['category1'];
	$dateFrom = $dateTo = "";
}

// Add New Entry button clicked
if(isset($_POST['addEntry'])){
	unset($_SESSION['entryAdded']);
}

?>

<html>
   
   <head>

		<title>Diary Page</title>
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script>
			$( function() {
				$( ".datepicker" ).datepicker();
			} );
		</script>

  		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<style type="text/css">
			input{
				display: block;
			}
			.datepicker, .inlineBtn, .filterSection{
				display: inline-block;
			}
			.item{
				background-color: #eee;
				border: 1px solid #000;
				margin-bottom: 20px;
			}
		</style>
      
   </head>
   
   <body bgcolor = "#FFFFFF">
	<div class="greeting">Hi <?php echo $_SESSION['login_user']?></div>
	<a href="logout.php">LOG OUT</a>
      <div align = "center">
		<?php if(!isset($_SESSION['entryAdded'])): ?>
			<div class="addEntry">
				<div><b>Add an entry</b></div>
					
				<div style = "margin:30px">
				   
				   <form action = "" method = "post" enctype="multipart/form-data">
				   		<p>Date: <input type="text" class="datepicker" name="date" value="<?php echo $date; ?>"></p>
				   		<textarea id="freeText" name="freeText" rows="4" cols="50"><?php echo $freeText; ?></textarea>
				       	<input type='file' name='image' />
						<?php if(isset($imgError)): ?>
						   	<div style = "font-size:11px; color:#cc0000; margin-top:10px"><?php echo $imgError; ?></div>
						<?php endif; ?>
						<p>Choose Category</p>
						<select id="category" name="category">
						  <option value="1" <?php if($category == 1) echo "selected"?> >Birthday</option>
						  <option value="2" <?php if($category == 2) echo "selected"?> >Wedding</option>
						  <option value="3" <?php if($category == 3) echo "selected"?> >Holiday</option>
						</select>
				        <input type="submit" value="Save" name="saveEntry" /><br />
				   </form>
				   <?php if(isset($emptyFieldError)): ?>
					   	<div style = "font-size:11px; color:#cc0000; margin-top:10px"><?php echo $emptyFieldError; ?></div>
					<?php endif; ?>
				   
				   <?php if(isset($error)): ?>
				   	<div style = "font-size:11px; color:#cc0000; margin-top:10px"><?php echo $error; ?></div>
				   <?php endif; ?>

				</div>
			</div>
		<?php else: ?>
			<div class="diary">
				<?php if(isset($showMsg)): ?>
				<div class="msg"> The diary entry is successfully added! </div>
				<?php endif; ?>
				<form action="" method="post">
					<div>
						<p class="filterSection">Filter by Date: from <input type="text" class="datepicker" name="dateFrom" value="<?php echo $dateFrom; ?>">
						 to <input type="text" class="datepicker" name="dateTo" value="<?php echo $dateTo; ?>"></p>
						<input type="submit" name="filterDate" value="Filter by date" class="inlineBtn">
					</div>
					<div>
						<p class="filterSection"> Filter by category: 
							<select id="category1" name="category1">
							  <option value="1" <?php if($category1 == 1) echo "selected"?> >Birthday</option>
							  <option value="2" <?php if($category1 == 2) echo "selected"?> >Wedding</option>
							  <option value="3" <?php if($category1 == 3) echo "selected"?> >Holiday</option>
							</select>
						</p>
						<input type="submit" name="filterCategory" value="Filter by category" class="inlineBtn">
					</div>
					<input type="submit" name="addEntry" value="Add New Entry">
				</form>

				
				<?php 
				// Filter by Date
				if(isset($_POST['filterDate'])){
					$sql = "SELECT E.date, E.freeText, E.image, C.categoryName FROM entries E INNER JOIN categories C on C.id = E.category WHERE E.userId = ".$_SESSION['user_id']." AND E.date between '".$dateFrom."' and '".$dateTo."' ORDER BY E.date";
					//print_r($sql);
				    $result = mysqli_query($db,$sql); ?>
				 	<div class="results">
					<?php while ($row = mysqli_fetch_assoc($result)) { //print_r($row);?>
						<div class="item">
						    <p>On <?php echo $row['date']; ?> you have <?php echo $row['categoryName']; ?></p>
						    <div><?php echo $row['freeText']; ?></div>
						    <?php if(!empty($row['image'])):?>
						    	<img src="<?php echo $target_dir.$row['image']?>">
						    <?php endif; ?>
						</div>
					<?php } ?>
					</div>
				<?php } ?>

				<?php 
				// Filter by Date
				if(isset($_POST['filterCategory'])){
					$sql = "SELECT E.date, E.freeText, E.image, C.categoryName FROM entries E INNER JOIN categories C on C.id = E.category WHERE E.userId = ".$_SESSION['user_id']." AND E.category = ".$_POST['category1']." ORDER BY E.date";
					//print_r($sql);
				    $result = mysqli_query($db,$sql); ?>
				 	<div class="results">
					<?php while ($row = mysqli_fetch_assoc($result)) { //print_r($row);?>
						<div class="item">
						    <p>On <?php echo $row['date']; ?> you have <?php echo $row['categoryName']; ?></p>
						    <div><?php echo $row['freeText']; ?></div>
						    <?php if(!empty($row['image'])):?>
						    	<img src="<?php echo $target_dir.$row['image']?>">
						    <?php endif; ?>
						</div>
					<?php } ?>
					</div>
				<?php } ?>

			</div>
		<?php endif; ?>
			
      </div>

   </body>
</html>